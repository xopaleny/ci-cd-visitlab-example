const request = require("supertest");
const app = require("./app.js");

describe("adding path", () => {
    test("GET /adding/number/number path response with sum", () => {
      return request(app)
        .get("/adding/2/2")
        .then(response => {
          expect(response.statusCode).toBe(200)
          expect(response.text).toBe("4")
        });
    });
  });
  

