const app = require("./app");
const port_fallback = 3000;
const port = process.env.PORT || port_fallback;

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});