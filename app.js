const express = require('express');
const math = require('./math.js')
const app = express();

app.get('/adding/:a/:b', (req, res) => {
  res.status(200).send(`${math.adding(parseInt(req.params.a), parseInt(req.params.b))}`)
});

module.exports = app;