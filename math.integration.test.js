const math = require('./math.js')

describe("additions", () => {
    
    test('adds 2 + 2 to equal 4', () => {
        expect(math.adding(2,2)).toBe(4)
    });

    test('adds 1 + 4 to equal 5', () => {
        expect(math.adding(1,4)).toBe(5)
    });

});